LH28F160 Amiga Kickstart Adapter
================================

Contained within this repository are two projects.

[LH28F160-Kickstart-Adapter](LH28F160-Kickstart-Adapter)
--------------------------------------------------------

This is a small PCB that adapts a **Samsung LH28F160S5T** 2 MB smart
flash chip in TSOP-56 package to the Amiga Kickstart's DIP-40 or DIP-42
footprint.

Main features are:

* The flash chip can be reprogrammed easily using the [LH28F160-Programming-Adapter](LH28F160-Programming-Adapter) (s.b.)
* Full 2 MB can be used in Amiga models that support it by using address lines A18 and A19.
* Switching between up to four different Kickstarts stored in one chip can be implemented very easily.

[LH28F160-Programming-Adapter](LH28F160-Programming-Adapter)
------------------------------------------------------------

This is an adapter that 
